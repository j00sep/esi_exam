package com.example.demo.inventory.domain.model;

import com.example.demo.common.domain.BusinessPeriod;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class PlantReservation {
    @Id
    @GeneratedValue
    Long id;

    @Embedded
    BusinessPeriod schedule;

    @ManyToOne
    PlantInventoryItem plant;

    Timestamp creationTime;
}
