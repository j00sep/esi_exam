package com.example.demo.inventory.application.dto;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class PlantReservationDTO extends ResourceSupport {
    Long _id;
    BusinessPeriod schedule;
    PlantInventoryItem plant;
    Timestamp creationTime;
}
